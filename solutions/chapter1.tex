\documentclass{article}
\usepackage[margin=1in]{geometry}
\usepackage{../hatcher}

\title{Chapter 1 Solutions}
\author{Jad Ghalayini}

\begin{document}

\maketitle

\section{Basic Constructions}

\begin{enumerate}

    \item Note that we assume homotopy is an equivalence relation  
    \begin{lemma}
        If \(g_0 \simeq g_1\), then for all \(f\), \(f \cdot g_0 \simeq f \cdot g_1\) and \(g_0 \cdot f \simeq g_1 \cdot f\)
        \label{lem:sides}
    \end{lemma}
    \begin{proof}
        If \(g_0 \simeq g_1\), we may find a homotopy \(G: g_0 \implies g_1\), allowing us to, given appropriate \(f\), define a homotopy
        \begin{equation}
            H(t) = f \cdot G(t) \implies H(0) = f \cdot g_0, H(1) = f \cdot g_1
        \end{equation}
        Hence, \(H: f \cdot g_0 \implies f \cdot g_1\) is a homotopy, as desired.

        Similarly, given appropriate \(f\), we may define a homotopy
        \begin{equation}
            H(t) = G(t) \cdot f \implies H(0) = g_0 \cdot f, H(1) = g_1 \cdot f
        \end{equation}
        Hence, \(H: g_0 \cdot f \implies g_1 \cdot f\) is a homotopy, as desired.
    \end{proof}
    We hence have, letting \(c_x\) denote the constant path at \(x\),
    \begin{equation}
        g_0 \simeq g_1 \implies f_1 \cdot g_1 \simeq f_1 \cdot g_0
    \end{equation}
    and hence, as \(f_0 \cdot g_0 \simeq f_1 \cdot g_1\), we have
    \begin{equation}
        f_0 \cdot g_0 \simeq f_1 \cdot g_0 \implies (f_0 \cdot g_0) \cdot \ol{g_0} \simeq (f_1 \cdot g_0) \cdot \ol{g_0}
    \end{equation}
    We compute
    \begin{equation}
        (f_i \cdot g_0) \cdot \ol{g_0} \simeq f_i \cdot (g_0 \cdot \ol{g_0}) \simeq f_i \cdot c_{f_i(1)} \simeq f_i \implies f_0 \simeq f_1
    \end{equation}
    as desired.

    \item If \(h \simeq h'\), then \(\ol{h} \simeq \ol{h'}\), and hence by Lemma \ref{lem:sides}
    \begin{equation}
        h \cdot (f \cdot \ol{h}) 
        \simeq h' \cdot (f \cdot \ol{h}) 
        \simeq (h' \cdot f) \cdot \ol{h}
        \simeq (h' \cdot f) \cdot \ol{h'}
        \simeq h' \cdot (f \cdot \ol{h'})
    \end{equation}
    It follows that
    \begin{equation}
        \forall f \in \pi_1(X, x_0), \beta_h(f) = [h \cdot f \cdot \ol{h}] = [h' \cdot f \cdot \ol{h'}] = \beta_{h'}(f)
    \end{equation}
    and hence that \(\beta_h\) depends only on the homotopy class of \(h\), as desired.

    \item Let \(X\) be a path connected space.
    \begin{itemize}

        \item \(\implies\): assume all basepoint change homomorphisms \(\beta_h\) depend only on the endpoints of the path \(h\). In particular, we then have that, fixing \(x_0\) and letting \(c\) be the constant path at \(x_0\),
        \begin{equation*}
            \forall [g] \in \pi(X, x_0), c(0) = c(1) = g(0) = g(1) = x_0 \implies [gfg^{-1}] = \beta_g(f) = \beta_c(f) = [cfc^{-1}] = [f]
        \end{equation*}
        Therefore,
        \begin{equation*}
            [f][g] = [gf\ol{g}][g] = [gf] = [g][f] = [f][g]
        \end{equation*}
        i.e. \(\pi(X) \simeq \pi(X, x_0)\) is abelian, as desired.

        \item \(\impliedby\) let \(h, h'\) be paths from \(x_0\) to \(x_1\), and assume \(\pi(X)\) is abelian. Then
        \begin{equation*}
            \beta_{h'}\beta_{\ol{h}} 
            = [h'\ol{h}fh\ol{h'}] 
            = [h'\ol{h}][f][h\ol{h'}]
            = [h'\ol{h}][h\ol{h'}][f]
            = [c][f]
            = [f]
        \end{equation*}
        Hence, \(\beta_{h'} = \beta_{\ol{h}}^{-1}\). Similarly,
        \begin{equation*}
            \beta_{h}\beta_{\ol{h}} =
            = [h\ol{h}fh\ol{h}] = [cfc] = [f] 
        \end{equation*}
        and hence \(\beta_{h} = \beta_{\ol{h}}^{-1}\), giving \(\beta_h = \beta_{h'}\), as desired.

    \end{itemize}

    \item \ \TODO{star shaped}

    \item \begin{itemize}

        \item (c) \(\implies\) (a): We may consider elements of \(\pi_1(X, x_0)\) to be homotopy classes of maps \(S^1 \to X\) with \(x_0\) in their image. It follows that, assuming (c), there is only one homotopy class of maps \(S^1 \to X\) containing \(x_0\), i.e. that all such maps are homotopic and hence in particular homotopic to the constant map \(x_0\). It follows that, as the image of every map from \(S^1\) is nonempty and therefore contains \textit{some} point \(s_0\), every map from \(S^1\) into \(X\) is homotopic to a constant map, i.e. (a).

        \item (a) \(\implies\) (c) if every map \(S^1 \to X\) is homotopic to a constant map with image a point, given a path \(p: [0, 1] \to X\) with \(p(0) = p(1) = x_0\) (which we may regard as a map \(S^1 \to X\)), this map is homotopic to a constant map with image a point \(x_1\) by (a). As \(x_1, x_0\) are connected by a path (e.g., if \(H: p \implies x_1\) is a homotopy, \(t \mapsto H(t)(0)\)), it follows that the constant map \(x_1\) is homotopic to the constant map \(x_0\), and hence that \(p\) is homotopic to \(x_0\),and hence in the homotopy class \(0 \in \pi_1(X, x_0)\). It follows that this is the only homotopy class, i.e. \(\pi_1(X, x_0) = 0\).

        \item (b) \(\implies\) (a): given a map \(f: S^1 \to X\), by (b), we may find a map \(f': D^2 \to X\) extending it. Writing \(f(r, \theta)\) to be the value of \(f\) in radial coordinates \(r \in [0, 1]\), \(\theta \in [0, 2\pi]\), we may define a continuous map
        \begin{equation}
            H(r)(\theta) = f'(r, \theta): [0, 1] \to [0, 2\pi] \to X
        \end{equation}
        As \(\forall r, H(r)(0) = H(r)(2\pi)\) by the properties of radial coordinates, this trivially quotients to a map \(H': [0, 1] \to S^1 \to X\); as \(H(0) = f'(1, \theta) = f\) (by extension) and \(H(1) = \theta \mapsto f(0, \theta)\) is constant (by the properties of radial coordinates), it follows that \(H'\) is a homotopy from \(f\) to a constant map \(S^1 \to X\), as desired.

        \item (a) \(\implies\) (b): given a map \(f: S^1 \to X\), let \(H\) be a homotopy from \(f\) to a constant map. We define a function \(f': D^2 \to X\) as follows:
        \begin{equation*}
            f'(r, \theta) = \left\{\begin{array}{cc}
                H(2 - 2r)(\theta) & \text{if} r \geq 1/2 \\
                H(1)(\theta) & \text{if} r \leq 1/2
            \end{array}\right.
        \end{equation*}
        We have \(f'(1, \theta) = f(\theta)\), and this function is well-defined and continuous as \(H(1)(\theta)\) is a constant (so \(f(0, \theta)\) does not depend on \(\theta\) as \(0 \leq 1/2\)).

    \end{itemize}
    \TODO{Clean this up...}

    \item \begin{itemize}

        \item Assume \(X\) is path connected. Then given maps \(f, g: X \to S^1\), we may use the change-of-basepoint transformation to obtain maps \(f', g'\) with \(f'(s_0) = g'(s_0) = x_0 \in X\). We trivially have \(f \simeq g \iff f' \simeq g'\), and hence that this induces a function \(\Phi': [S^1, X] \to \pi_1(X, x_0)\). We furthermore have that, ignoring basepoints,
        \begin{equation}
            \Phi'(f) \simeq f \implies \Phi(\Phi'([f])) = [f] \implies \Phi \circ \Phi' = \id
        \end{equation}
        and hence that \(\Phi\) is onto.
        
        \TODO{clean}

        \item Given maps \(f, g: X \to S^1\) with \(f(s_0) = g(s_0) = x_0\), assume \(\Phi([f]) = \Phi([g])\). It follows that \(f\) may be deformed to \(g\) in \(S^1\). We may convert this to a basepoint preserving homotopy as follows: \TODO{basepoint preserving}
    \end{itemize}

    \item \ \TODO{Conditions on homotopy...}

    \item \ \TODO{Borsuk-Ulam...}

    \item \ \TODO{Borsuk-Ulam slicing... this was on the exam, wasn't it... but 1300...}

    \item \ \TODO{Loops}

    \item \ \TODO{Inclusion iso}

    \item \ \TODO{Induced hom}

    \item \ \TODO{Surjection from subset group... I remember this one...}

    \item \ \TODO{Explicit iso from Cartesian product}

    \item \ \TODO{Commutative diagrams}

    \item \begin{enumerate}

        \item \ \TODO{\(\reals^3\) and \(S^1\): \(0\) and \(\ints\)}

        \item \ \TODO[Filled torus and boundary torus: \(\ints\) and \(\ints^2\)]

        \item \ \TODO{Filled torus and internal knot: \(\ints\) and... well... it's \(\ints\), so something more subtle is wrong...}

        \item \ \TODO{Wedged disks, wedged circles}

        \item \ \TODO{Disk, wedged circles}

        \item \ \TODO{Mobius}

    \end{enumerate}

    \TODO{fill this in}

    \item

    \item

    \item

    \item

\end{enumerate}

\section{Van Kampen's Theorem}

\begin{enumerate}

    \item Let \(G, H\) be nontrivial, and assume \(z \in Z(G \ast H)\). We have that \(z = \prod_ig_ih_i\) for \(g_i \in G\), \(h_i \in H\); furthermore, we have
    \begin{equation}
        \forall g \in G, gz = zg \implies ... 
    \end{equation}
    \TODO{this...}

    \item \ \TODO{copy from old solutions? Proposition 1.26?}

    \item \ \TODO{copy from old solutions?}

    \item Let \(Y = \reals^3 - X\), where \(X\) the union of \(n\) lines through the origin. We claim \(\pi_1(\reals^3 - X) = \ints^n\)
    
    \TODO{Van Kampen}

    \item \ \TODO{Some graph theory}

    \item \ \TODO{Proposition 1.26?}

    \item \ \TODO{Cell structure}

    \item \ \TODO{this}

    \item \ \TODO{genii again... copy old solution?}

    \item \ \TODO{the arcs... copy old solution?}

    \item \ \TODO{Klein}

    \item \ \TODO{More Klein}

    \item \ \TODO{the cube... copy old solution?}

    \item \ \TODO{this}
    
    \item \ \TODO{Induction?}

    \item \ \TODO{Uncountability... disconnected-ish? No...}

    \item \ \TODO{Suspension}

    \item \ \TODO{Simply connected spheres}

    \item \ \TODO{Infinite circles... Hawaii?}
 
    \item \ \TODO{Simply connected join of path connected spaces}

    \item \ \TODO{Wirtinger presentation}

\end{enumerate}

\section{Covering Spaces}

\begin{enumerate}

    \item Let \(p: \tilde X \to X\) be a covering space and \(A \subset X\) be a subspace, and let \(\tilde A = p^{-1}(A)\).
    
    \TODO{\(p: \tilde A \to A\) is a covering space}

    \item Let \(p_i: \tilde X_i \to X\) be covering spaces for \(i \in \{1, 2\}\).
    
    \TODO{\(p_1 \times p_2: \tilde X_1 \times \tilde X_2 \to X_1 \times X_2\) is a covering space}

    \item Let \(p: \tilde X \to X\) be a covering space with \(p^{-1}(x)\). \TODO{Hausdorff}

    \item \ \TODO{Simply connected covering space}

    \item \ \TODO{Subspace}

    \item \ \TODO{Shrinking wedge of circles}

\end{enumerate}

\section{Graphs and Free Groups}

\begin{enumerate}

    \item \ \TODO{Weak topology...}

\end{enumerate}

\section{\texorpdfstring{\(K(G, 1)\)}{K(G, 1)} Spaces and Graphs of Groups}


\begin{enumerate}

    \item \ \TODO{Simplicial action}

\end{enumerate}


\end{document}