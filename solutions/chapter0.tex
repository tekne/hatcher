\documentclass{article}
\usepackage[margin=1in]{geometry}
\usepackage{../hatcher}

\title{Chapter 0 Solutions}
\author{Jad Ghalayini}

\begin{document}

\maketitle

\begin{enumerate}

    \item Let \(X\) be the square ABCD in Figure \ref{fig:torus-minus}, with \(\tilde X\) being the quotient of \(X\) to obtain the torus, and let \(q\) be the quotient map and \(A = q(\partial X)\). We define a metric
    \begin{equation}
        d(x, y) = |x| + |y| \qquad \implies \qquad X = \{d(p) \leq 1: p \in \reals^2\}, \quad \forall p \in \reals^2, d(ap) = ad(p)
    \end{equation}
    We may hence define a continuous function \(f: [0, 1] \to X \to X\) given by
    \begin{equation}
        f(t)(p) = \frac{p}{(1 - t) + td(p)}
    \end{equation}
    The target of this function is indeed \(X\), since
    \begin{equation*}
        \forall p \in X, d(p) \leq 1 \implies (1 - t) + td(p) \geq d(p) \implies d(f(t)(p)) = \frac{d(p)}{(1 - t) + td(p)} \leq \frac{d(p)}{d(p)} = 1
    \end{equation*}
    Furthermore, we have that
    \begin{equation*}
        \forall p \in X, f(0)(p) = \frac{p}{1} = p \implies f(0) = \id
    \end{equation*}
    \begin{equation*}
        f(1)(p) = \frac{p}{d(p)} \implies d(f(1)(p)) = \frac{d(p)}{d(p)} = 1 \implies f(1)(p) \in \partial X \implies f(1): X \to \partial X
    \end{equation*}
    \begin{equation*}
        \forall t \in [0, 1], p \in \partial X, d(p) = 1 \implies f(t)(p) = \frac{p}{(1 - t) + td(p)} = \frac{p}{1 - t + t} = p \implies f(t)|_{\partial X} = \id_{\partial X}
    \end{equation*}
    Hence, \(f\) quotients trivially to a continuous function \(\tilde f: [0, 1] \to \tilde X \to \tilde X\) such that \(f(0) = \id_{\tilde X}\), \(f(1): \tilde X \to A\), and \(\forall t \in [0, 1], \tilde f(t)|_{A} = \id_A\), i.e. a deformation retraction.

    \begin{figure}
        \centering
        \begin{tikzpicture}
            \node[circle, fill, inner sep = 2pt, label=above:{\(A = (0, 1)\)}] at (0, 2) (A) {};
            \node[circle, fill, inner sep = 2pt, label=left:{\(B = (-1, 0)\)}] at (-2, 0) (B) {};
            \node[circle, fill, inner sep = 2pt, label=below:{\(C = (0, -1)\)}] at (0, -2) (C) {};
            \node[circle, fill, inner sep = 2pt, label=right:{\(D = (1, 0)\)}] at (2, 0) (D) {};
            \node[circle, fill=white, draw, inner sep = 2pt, label=left:{\((0, 0)\)}] at (0, 0) (O) {};

            \draw[->, red] (A) -- (-1, 1);
            \draw[red] (-1, 1) -- (B);
            \draw[->, blue] (B) -- (-1, -1);
            \draw[blue] (-1, -1) -- (C);
            \draw[->, red] (D) -- (1, -1);
            \draw[red] (1, -1) -- (C);
            \draw[->, blue] (A) -- (1, 1);
            \draw[blue] (1, 1) -- (D);

        \end{tikzpicture}
        \caption{
            A square \(ABCD\) having edge length \(2\) and centered at the origin, minus the point at the origin. If \(AB\) is glued to \(DC\) (red) and \(AD\) glued to \(BC\) (blue) with the orientation shown in the diagram, we obtain the torus, with the blue and red edges corresponding to the longitude and meridian circles of the torus.
        }
        \label{fig:torus-minus}
    \end{figure}

    \item Let \(X = \reals^n - \{p\}\) and consider the function \(f: [0, 1] \to X \to X\) given by
    \begin{equation}
        f(t, p) = \frac{p}{(1 - t) + t|p|}
    \end{equation}
    We have \(f(0)(p) = p \implies f(0) = \id\), and \(f(1)(p) = \frac{p}{|p|} \in S^{n - 1}\), furthermore, we have
    \begin{equation}
        \forall p \in S^{n - 1}, |p| = 1 \implies f(t)(p) = \frac{p}{(1 - t) + t|p|} = \frac{p}{1 - t + t} = p \implies f(t)|_{S^{n - 1}} = \id
    \end{equation}
    Hence, \(f\) is a deformation retraction from \(X\) to \(S^{n - 1}\), as desired.

    \item \begin{enumerate}[label=(\alph*)]

        \item Let \(g: X \to Y\) and \(f: Y \to Z\) be homotopy equivalences; we may hence find maps \(g': Y \to X, f': Z \to Y\) such that there exist homotopies \(F: ff' \implies \id\), \(F': f'f \implies \id\), \(G: gg' \implies \id\), \(G': g'g \implies \id\). It follows that we may construct maps \(H: [0, 1] \to Z \to Z\), \(H': [0, 1] \to X \to X\) given by
        \begin{equation}
            H(t) = \left\{\begin{array}{cc}
                F(2t)        & \text{if} \ t \leq 1/2 \\
                fG(2t - 1)f' & \text{if} \ t \geq 1/2
            \end{array}\right.
            , \quad H'(t) = \left\{\begin{array}{cc}
                G'(2t)        & \text{if} \ t \leq 1/2 \\
                gF'(2t - 1)g' & \text{if} \ t \geq 1/2
            \end{array}\right.
        \end{equation}
        These maps are continuous and well-defined since
        \begin{equation*}
            H(1/2) = F(1) = ff' = f \id f' = fG(0)f', \quad H'(1/2) = G'(1) = gg' = g \id g' = g F'(0) g'
        \end{equation*}
        Furthermore,
        \begin{equation}
            H(0) = fgg'f': Z \to Z, \ H(1) = \id_Z, \ H'(0) = g'f'fg: X \to X, \ H'(1) = \id_X
        \end{equation}
        i.e. \(H: fgg'f' \implies \id\), \(H': g'f'fg \implies \id\) are homotopies. It follows that \(fg: X \to Z\) is a homotopy equivalence with homotopy inverse \(g'f': Z \to X\), as desired.

        \item We prove the axioms for an equivalence relation as follows: let \(f, g, h: X \to Y\) be maps.
        \begin{itemize}
            \item (Reflexivity) \(F: [0, 1] \to X \to Y\) given by \(F(t) = f\) is trivially a continuous map, and hence a homotopy \(F: f \implies f\); it follows that \(f\) is homotopic to \(f\), and hence in general that homotopy is reflexive.
            \item (Transitivity) If \(F: f \implies g\) and \(G: g \implies h\) are homotopies, then we may define a map \(H: [0, 1] \to X \to Y\) given by
            \begin{equation}
                H(t) = \left\{\begin{array}{cc}
                    F(2t)     & \text{if} \ t \leq 1/2 \\
                    G(2t - 1) & \text{if} \ t \geq 1/2
                \end{array}\right.
            \end{equation}
            This map is continuous and well-defined since
            \begin{equation}
                H(1/2) = F(1) = G(0) = g
            \end{equation}
            Furthermore,
            \begin{equation}
                H(0) = F(0) = f, \qquad H(1) = G(1) = h
            \end{equation}
            implying \(H: f \implies h\) is a homotopy, i.e. \(f\) is homotopic to \(h\), and hence in general that homotopy is transitive.
            \item (Symmetry) Let \(f, g: X \to Y\) and \(F: f \implies g\) be a homotopy. We may define a continuous function
            \begin{equation*}
                G(t) = F(1 - t)
            \end{equation*}
            We trivially have \(G(0) = F(1) = g\), \(G(1) = F(0) = f\), and hence that \(G: g \implies f\) is a homotopy, i.e. \(g\) is homotopic to \(f\), and hence in general that homotopy is symmetric.
        \end{itemize}
        \label{part:homotopy-equiv}

        \item Let \(f: X \to Y\) be a homotopy equivalence and let \(g: X \to Y\) be a map homotopic to \(f\). We then have a map \(f': Y \to X\) such that there exist homotopies \(F: ff' \implies \id\) and \(F': f'f \implies \id\). As \(f\) is homotopic to \(g\), by part \ref{part:homotopy-equiv} of this question, we may find a homotopy \(H: g \implies f\). It follows that we may construct maps
        \begin{equation}
            G(t) = \left\{\begin{array}{cc}
                H(2t)f'     & \text{if} \ t \leq 1/2 \\
                F(2t - 1) & \text{if} \ t \geq 1/2
            \end{array}\right., \qquad
            G'(t) = \left\{\begin{array}{cc}
                f'H(2t)     & \text{if} \ t \leq 1/2 \\
                F'(2t - 1) & \text{if} \ t \geq 1/2
            \end{array}\right.
        \end{equation}
        These maps are continuous and well-defined, since
        \begin{equation}
            G(1/2) = H(1)f' = F(0) = ff', \qquad G'(1/2) = f'H(1) = F'(0) = f'f
        \end{equation}
        Furthermore, we have \(G(0) = gf', G(1) = F(1) = \id\), \(G'(0) = f'g\), \(G'(1) = F'(1) = \id\), i.e. that \(G: gf' \implies \id\) and \(G': f'g \implies \id\) are homotopies and hence that \(g\) is a homotopy equivalence, as desired.

    \end{enumerate}

    \item Let \(F(t) = f_t\) be a deformation retraction in the weak sense from \(X\) to \(A \subset X\) and \(i: A \to X\) be the inclusion map. We may hence define continuous maps
    \begin{equation}
        I(t) = F(1 - t)i, \qquad I'(t) = (F(t))_AF(1)
    \end{equation}
    We note that, for all \(t\), \(\forall a \in A, I(t)(a) = F(t)(a) \in A\), and hence \(I(t): A \to A\). We furthermore have
    \begin{equation}
        I(0) = F(1)i, \quad I(1) = F(0)i = \id_A, \quad I'(0) = F(0)|_A  F(1) = iF(1), \quad I'(1) =  F(1)_AF(1)
    \end{equation}
    \TODO{fixme}

\end{enumerate}

\end{document}